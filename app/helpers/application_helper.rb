module ApplicationHelper
  def title(page_title)
    content_for(:title) { page_title }
  end
  def in_edit_view?
    'graphs/edit' == "#{controller_name}/#{action_name}" 
  end
end
