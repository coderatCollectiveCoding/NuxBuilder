class Graph < ApplicationRecord
  validates :token, presence: true, uniqueness: true
  validates :data, presence: true
end
