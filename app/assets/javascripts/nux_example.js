jQuery(function() {
  jQuery('#nux').each(function() {
    const data = {
      "data": [{
        "x": -377,
        "y": -247,
        "id": "1",
        "connections": [
          "2",
          "3"
        ],
        "label": "You are hungry? What do you prefer?",
        "group": "rootStatement"
      }, {
        "x": -539,
        "y": -136,
        "id": "2",
        "connections": [
          "1",
          "4"
        ],
        "label": "Salty stuff",
        "group": "choice"
      }, {
        "x": -300,
        "y": -144,
        "id": "3",
        "connections": [
          "1",
          "11"
        ],
        "label": "Sweet stuff",
        "group": "choice"
      }, {
        "x": -591,
        "y": 0,
        "id": "4",
        "connections": [
          "2",
          "5",
          "6",
          "7"
        ],
        "label": "There is so much. Please specify.",
        "group": "statement"
      }, {
        "x": -750,
        "y": 89,
        "id": "5",
        "connections": [
          "4",
          "8"
        ],
        "label": "Give me something greasy!",
        "group": "choice"
      }, {
        "x": -503,
        "y": 136,
        "id": "6",
        "connections": [
          "4",
          "9"
        ],
        "label": "I like super healthy power food! ",
        "group": "choice"
      }, {
        "x": -476,
        "y": 67,
        "id": "7",
        "connections": [
          "4",
          "10"
        ],
        "label": "I need just a little snack.",
        "group": "choice"
      }, {
        "x": -663,
        "y": 218,
        "id": "8",
        "connections": [
          "5"
        ],
        "label": "What's about Pommes Rot Weiß?!",
        "group": "statement"
      }, {
        "x": -501,
        "y": 233,
        "id": "9",
        "connections": [
          "6"
        ],
        "label": "This is the wrong tool for you ;)",
        "group": "statement"
      }, {
        "x": -516,
        "y": 300,
        "id": "10",
        "connections": [
          "7"
        ],
        "label": "Eat some nux, ehm, nuts.",
        "group": "statement"
      }, {
        "x": -274,
        "y": -24,
        "id": "11",
        "connections": [
          "3",
          "12",
          "15",
          "16"
        ],
        "label": "There is so much. Please specify.",
        "group": "statement"
      }, {
        "x": -304,
        "y": 94,
        "id": "12",
        "connections": [
          "11",
          "13"
        ],
        "label": "Real unhealthy candy, please!",
        "group": "choice"
      }, {
        "x": -319,
        "y": 228,
        "id": "13",
        "connections": [
          "12"
        ],
        "label": "What's about schocolade?",
        "group": "statement"
      }, {
        "x": -19,
        "y": 230,
        "id": "14",
        "connections": [
          "15"
        ],
        "label": "Ananas?",
        "group": "statement"
      }, {
        "x": -71,
        "y": 99,
        "id": "15",
        "connections": [
          "11",
          "14"
        ],
        "label": "Sweet and Fruity!",
        "group": "choice"
      }, {
        "x": 155,
        "y": 104,
        "id": "16",
        "connections": [
          "11",
          "17"
        ],
        "label": "I prefer salty stuff, after all.",
        "group": "choice"
      }, {
        "x": 135,
        "y": 269,
        "id": "17",
        "connections": [
          "16"
        ],
        "label": "Come on....",
        "group": "statement"
      }]
    };

    // initialize nux and render within a div of your choice
    new Nux(data, '#nux');
  });
});