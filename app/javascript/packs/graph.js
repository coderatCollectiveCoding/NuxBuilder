import * as importHelpers from './modules/import_helpers.js';
import * as exportHelpers from './modules/export_helpers.js';
import * as popUp from './modules/popup.js';
import * as editorStyle from './modules/editor_style.js';
import * as encryption from './modules/encryption.js';
import * as helpers from './modules/helpers.js';
import {
  validateTree
} from './modules/validation.js';

$(function() {
  window.network = {};

  function greatestNodeId(network) {
    const nodeIds = $.map(network.body.nodes, function(n) {
      return parseInt(n.id)
    });
    return Math.max.apply(Math, nodeIds)
  };

  function shallEncrypt() {
    return document.getElementById('shall-encrypt').checked;
  }

  function pushToBackend(data) {
    // Use that to prevent csrf-token missing error
    $.ajax({
      type: "PATCH",
      url: `/graphs/${window.graphToken}`,
      data: {
        data: data,
        encrypted: shallEncrypt()
      },
      success: function(response) {
        helpers.showMessage({
          text: 'Tree successfully saved!',
          type: 'success'
        })
      },
      error: function(response) {
        helpers.showMessage({
          text: 'Snap, something went wrong saving your tree!',
          type: 'danger'
        })
      }
    })
  }

  function encryptAndPushToBackend({
    text,
    passphrase
  }) {
    const cipheredData = encryption.encrypt({
      text: text,
      passphrase: passphrase
    });
    pushToBackend(cipheredData);
    savePasswordModal.modal('hide');
  }

  function promptPasswordAndDecrypt(input) {
    loadPasswordModal.modal('show');
    $('#load-password-prompt #load').on('click', () => {
      var passphrase = $('#load-password-prompt #password').val();
      var plainData = encryption.decrypt({
        text: input,
        passphrase: passphrase
      });
      setDataAndDrawTree(plainData);
    })
  }

  function rawData() {
    const nodes = document.getElementById('raw-data-textarea').value;
    // return `{"data": ${nodes}}`;
    return nodes;
  }

  function draw(data) {
    let nodes = [];
    let edges = [];

    // create a network
    var container = document.getElementById('graph');

    var editorOptions = {
      manipulation: {
        initiallyActive: true,
        addNode: function(data, callback) {
          const greatestId = greatestNodeId(network);
          // filling in the popup DOM elements
          document.getElementById('operation').innerHTML = "Add something";
          document.getElementById('node-id').value = greatestId + 1;
          document.getElementById('node-label').value = '';
          document.getElementById('save-button').onclick = popUp.saveData.bind(this, data, callback);
          document.getElementById('cancel-button').onclick = popUp.clearPopUp.bind();
          document.getElementById('network-popUp').style.display = 'block';
        },
        editNode: function(data, callback) {
          // filling in the popup DOM elements
          document.getElementById('operation').innerHTML = "Edit " + data.group;
          document.getElementById('node-id').value = data.id;
          document.getElementById('node-label').value = data.label;
          document.getElementById('node-group').value = data.group;
          document.getElementById('save-button').onclick = popUp.saveData.bind(this, data, callback);
          document.getElementById('cancel-button').onclick = popUp.cancelEdit.bind(this, callback);
          document.getElementById('network-popUp').style.display = 'block';
        },
        addEdge: function(data, callback) {
          if (data.from == data.to) {
            alert("Drag to another node!")
          } else {
            validateTree(network, data, callback)
          };
        },
        deleteEdge: function(data, callback) {
          validateTree(network, data, callback)
        },
        deleteNode: function(data, callback) {
          validateTree(network, data, callback)
        },
      }
    };

    var options = Object.assign(editorStyle.styleOptions, editorOptions);
    $('#builder').show();
    network = new vis.Network(container, data, options);
    network.on('release', function() {
      validateTree(network, null, null)
    })
    validateTree(network, null, null);
  }

  const loadPasswordModal = $('.modal#load-password-prompt')
  const savePasswordModal = $('.modal#save-password-prompt')

  function setDataAndDrawTree(input) {

    try {
      window.data = {
        nodes: importHelpers.getNodeData(input.data),
        edges: importHelpers.getEdgeData(input.data)
      }

      draw(data);
    } catch (error) {
      helpers.showMessage({
        text: 'Snap, there seems to be something wrong with your data!',
        type: 'danger'
      })
    }
  }

  // Draw tree from given Data, if that doesnt work prompt for password
  $('#builder').hide();
  if (window.encrypted) {
    promptPasswordAndDecrypt(window.graphData)
  } else {
    setDataAndDrawTree(window.graphData);
  }

  // ACTION BUTTON BINDINGS
  document.getElementById('push-to-backend-button').addEventListener('click', () => {
    if (shallEncrypt()) {
      savePasswordModal.modal('show');
    } else {
      pushToBackend(rawData());
    }
  })

  // import via text field
  $('#raw-data-textarea').bind('keyup keydown', function(e) {
    // do only allow str+c, strg+a and strg+v
    if ((e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 67) && (e.ctrlKey === true || e.metaKey === true)) {
      return
    } else {
      e.preventDefault();
    }
  });

  var textArea = $('#raw-data-textarea');
  textArea.data('copyOfVal', textArea.val());
  textArea.on('input', function() {
    var oldInput = $(this).text();
    try {
      var input = JSON.parse($(this).val());
      var data = {
        nodes: importHelpers.getNodeData(input['data']),
        edges: importHelpers.getEdgeData(input['data'])
      };
      draw(data);
      $(this).data('copyOfVal', $(this).val());
    } catch (err) {
      // jump back to previous value and raise error message unless json is valid
      $('#raw-data-textarea').val($(this).data('copyOfVal'));
      alert('Inserted JSON is not valid!')
    }
  });

  function validatePassword() {
    const password = document.getElementById('password').value;
    const passwordConfirmation = document.getElementById('password-confirmation').value;

    function passwordsMatch() {
      return password == passwordConfirmation;
    }

    function validPassword() {
      return passwordsMatch() && password.length >= 6;
    }

    document.getElementById('encrypt-and-push-to-backend').disabled = !validPassword();
  }

  document.getElementById('password').addEventListener('keyup', () => {
    encryption.validatePassword();
  })

  document.getElementById('password-confirmation').addEventListener('keyup', () => {
    encryption.validatePassword();
  })

  document.getElementById('encrypt-and-push-to-backend').addEventListener('click', () => {
    const password = document.getElementById('password').value;
    encryptAndPushToBackend({
      text: rawData(),
      passphrase: password
    });
  })

  // Toggle encryption
  document.getElementById('shall-encrypt').addEventListener('change', () => {
    window.encrypted = this.checked;
  })
});