function showMessage({text, type}) {
  const messageDiv = document.querySelector('#nux-messages');
  messageDiv.innerHTML = text;
  messageDiv.classList = [`text-${type}`];
}

export {
  showMessage
}
