// Code copy & pasted from vis network example
// => Helps converting data from single-hash-format into separate nodes and edges array format
function getNodeData(data) {
  var networkNodes = [];

  data.forEach(function(elem, index, array) {
    networkNodes.push({
      id: elem.id,
      label: elem.label,
      group: elem.group,
      x: elem.x,
      y: elem.y,
    });
  });

  return new vis.DataSet(networkNodes);
}

function getNodeById(data, id) {
  for (var n = 0; n < data.length; n++) {
    if (data[n].id == id) { // double equals since id can be numeric or string
      return data[n];
    }
  };

  throw 'Can not find id \'' + id + '\' in data';
}

function getEdgeData(data) {
  var networkEdges = [];

  data.forEach(function(node) {
    // add the connection
    node.connections.forEach(function(connId, cIndex, conns) {
      networkEdges.push({
        from: node.id,
        to: connId
      });
      let cNode = getNodeById(data, connId);

      var elementConnections = cNode.connections;

      // remove the connection from the other node to prevent duplicate connections
      var duplicateIndex = elementConnections.indexOf(node.id);

      if (duplicateIndex != -1) {
        elementConnections.splice(duplicateIndex, 1);
      };
    });
  });

  return new vis.DataSet(networkEdges);
}

export {
  getNodeData,
  getNodeById,
  getEdgeData
}