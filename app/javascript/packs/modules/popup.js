import { validateTree } from './validation.js';
import * as exportHelpers from './export_helpers.js';

function clearPopUp() {
  document.getElementById('save-button').onclick = null;
  document.getElementById('cancel-button').onclick = null;
  document.getElementById('network-popUp').style.display = 'none';
}

function cancelEdit(callback) {
  clearPopUp();
  callback(null);
}

function saveData(data, callback) {
  data.id = document.getElementById('node-id').value;
  data.label = document.getElementById('node-label').value;
  data.group = document.getElementById('node-group').value;
  clearPopUp();
  validateTree(network, data, callback);
}

export {
  clearPopUp,
  cancelEdit,
  saveData
}