function nodeStyle(color) {
  return {
    font: {
      color: 'white',
      face: 'Lato, Helvetica Neue, Helvetica, Arial, sans-serif',
    },
    color: {
      highlight: {
        background: color,
        border: 'white',
      },
      background: color,
      border: 'grey',
    },
    labelHighlightBold: false,
    shape: 'box',
    shapeProperties: {
      borderRadius: 0
    },
    margin: 15,
    widthConstraint: {
      maximum: 300
    }
  }
}

const styleOptions = {
  locales: {
    en: {
      edit: 'Edit',
      del: 'Delete selected',
      back: 'Back',
      addNode: 'Add choice/statement',
      addEdge: 'Add connection',
      editNode: 'Edit selected',
      editEdge: 'Edit selected',
      addDescription: 'Click on empty space to place a new choice/statement.',
      edgeDescription: 'Click on a node and drag to another to connect them.',
      editEdgeDescription: 'Click on a node and drag to another to connect them.',
      createEdgeError: 'Cannot link edges to a cluster.',
      deleteClusterError: 'Clusters cannot be deleted.',
      editClusterError: 'Clusters cannot be edited.'
    },
  },
  physics: {
    enabled: false
  },
  edges: {
    smooth: false,
    selectionWidth: 1,
  },
  groups: {
    choice: nodeStyle('#df691a'),
    statement: nodeStyle('#39b3d7'),
    rootStatement: nodeStyle('#494F92'),
  }
};

export {
  styleOptions
};