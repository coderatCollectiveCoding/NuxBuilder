const CryptoJS = require("crypto-js");

function decrypt({
  text,
  passphrase
}) {
  const plaintext = CryptoJS.AES.decrypt(text, passphrase).toString(CryptoJS.enc.Latin1);
  try {
    let json = JSON.parse(plaintext);
    $('.modal#load-password-prompt').modal('hide');
    return json;
  } catch (error) {
    $('.wrong-password').show();
  };
}

function encrypt({
  text,
  passphrase
}) {
  const ciphertext = CryptoJS.AES.encrypt(text, passphrase).toString();

  return ciphertext;
}

function validatePassword() {
  const password = document.getElementById('password').value;
  const passwordConfirmation = document.getElementById('password-confirmation').value;

  function passwordsMatch() {
    return password == passwordConfirmation;
  }

  function validPassword() {
    return passwordsMatch() && password.length >= 6;
  }

  document.getElementById('encrypt-and-push-to-backend').disabled = !validPassword();
}

export {
  decrypt,
  encrypt,
  validatePassword
}