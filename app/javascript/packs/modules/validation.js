import * as exportHelpers from './export_helpers.js';
import * as helpers from './helpers.js';

function idInConnectionArray(id, array) {
  // check if array includes value regardless on wether string or integer type
  const integerizedArray = array.map((id) => parseInt(id));
  return integerizedArray.includes(parseInt(id))
}

function neighbors(id, nodes) {
  return nodes.filter((n) => {
    return idInConnectionArray(id, n.connections) && parseInt(n.id) != parseInt(id)
  });
};

function statementNeighbors(id, nodes) {
  return neighbors(id, nodes).filter((n) => {
    return (n.group == 'statement' || n.group == 'rootStatement')
  })
}

function choiceNeighbors(id, nodes) {
  return neighbors(id, nodes).filter((n) => {
    return n.group == 'choice'
  })
}

function hasMultipleTrees(nodes) {
  const rootNode = nodes.find((n) => n.group == 'rootStatement')
  const nodesWithoutParentReferences = removeParentReferences(rootNode, nodes);
  const idsOfRootNodeTree = getIdsOfRootNodeTree(rootNode, nodesWithoutParentReferences, [parseInt(rootNode.id)]);

  // compare ids fetched by the recursion of the root node tree
  // with the map of all ids. If there is no difference, only one tree exists
  const sortedIdsOfRootNodeTree = idsOfRootNodeTree.sort((a, b) => (a - b));
  const sortedNodes = nodes.map((n) => parseInt(n.id)).sort((a, b) => (a - b));
  // array contents cannot be compared directly, thus use their stringified representation
  return !(JSON.stringify(sortedIdsOfRootNodeTree) == JSON.stringify(sortedNodes))
}

function removeParentReferences(currentNode, nodes) {
  // Recursevly deletes nodes beginning with root statement
  // then looking for remaining nodes
  currentNode.connections.forEach(nextNodeId => {
    const nextNodeIndex = nodes.findIndex((node) => parseInt(node.id) == parseInt(nextNodeId));
    // Remove currentNode.id (-> parent) in next nodes and recall func for next currentNode
    const index = nodes[nextNodeIndex].connections.findIndex((id) => parseInt(id) == parseInt(currentNode.id));
    if (index !== -1) {
      nodes[nextNodeIndex].connections.splice(index, 1);
    }
    // Recall function
    removeParentReferences(nodes[nextNodeIndex], nodes);
  })
  return nodes;
}

function getIdsOfRootNodeTree(currentNode, nodes, ids) {
  currentNode.connections.forEach(nextNodeId => {
    ids.push(parseInt(nextNodeId));
    const nextNode = nodes.find((node) => parseInt(node.id) == parseInt(nextNodeId));

    // Recall function
    getIdsOfRootNodeTree(nextNode, nodes, ids);
  })
  return ids;
}

function validate(nodes) {
  let error = '';

  // check root statement rules
  const rootStatements = nodes.filter((n) => {
    return n.group == 'rootStatement'
  });
  if (rootStatements.length < 1) {
    error = 'There must be one root statement!'
  };
  if (rootStatements.length > 1) {
    error = 'There may be only one root statement!'
  };

  // loop through nodes and abort immediately with error message when rule is broken
  if (error == '') {
    nodes.forEach((n) => {
      if (n.connections.length == 0) {
        error = 'Each node must be connected!';
        return false;
      }
      if (n.connections.length > 2 && n.group == 'choice') {
        error = 'Choices may only be connected with two statements!';
        return false;
      }
      const choices = choiceNeighbors(n.id, nodes).length;
      const statements = statementNeighbors(n.id, nodes).length;
      if (n.group == 'choice' && choices > 0) {
        error = 'Choices must not be connected to other choices!';
        return false;
      }
      if (n.group == 'choice' && statements !== 2) {
        error = 'Choices must be connected to two statements!';
        return false;
      }
      if ((n.group == 'statement' || n.group == 'rootStatement') && statements > 0) {
        error = 'Statements must not be connected to other statments!';
        return false;
      }
      if (n.group == 'rootStatement' && choices < 1) {
        error = 'Root statements must have at least one choice!';
        return false;
      }
    })

    if (hasMultipleTrees(nodes)) {
      error = 'Only one tree allowed!';
    }
  }

  return error;
};


function validateTree(network, featureData, callback) {
  // save item
  if (featureData && callback) {
    callback(featureData);
  }

  // update preview code
  exportHelpers.viewRawNetworkData({
    network: network
  });

  // validate tree
  try {
    const nodes = exportHelpers.networkDataObject(network);
    const errorMessage = validate(nodes);
    helpers.showMessage({text: errorMessage, type: 'danger'})
  } catch (err) {
    // in case of self-references within tree, a stack depth error is raised.
    // catch this here and put generic error message
    helpers.showMessage({text: 'This is somehow invalid...', type: 'danger'})
  }
};

export {
  validateTree
}
