const dataViewArea = $('#raw-data-textarea');

function networkDataObject(network) {
  let nodesPositions = objectToArray(network.getPositions());
  let nodesData = objectToArray(network.body.data.nodes._data);
  nodesPositions.forEach((n) => {
    n.connections = network.getConnectedNodes(n.id);
    // add label and group to nodes objects
    var data = nodesData.find((d) => d.id == n.id)
    Object.assign(n, {
      label: data.label,
      group: data.group
    })
  });
  return nodesPositions;
}

function viewRawNetworkData({
  network
}) {
  const jsonString = JSON.stringify({
    data: networkDataObject(network)
  }, undefined, 2);
  // Paste to textarea
  dataViewArea.val(jsonString);
}

function rawNetworkData() {
  return $('#raw-data-textarea').text();
}

function objectToArray(obj) {
  return Object.keys(obj).map(function(key) {
    obj[key].id = key;
    return obj[key];
  });
}

export {
  networkDataObject,
  viewRawNetworkData,
  downloadRawData,
  rawNetworkData
}