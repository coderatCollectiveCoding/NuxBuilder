require 'securerandom'

class GraphsController < ApplicationController
  def new
  end

  def create
    graph = Graph.new(token: SecureRandom.uuid, encrypted: false)

    if graph.save
      redirect_to edit_graph_path(graph.token)
    else
      flash[:danger] = graph.errors.full_messages.to_sentence
      redirect_to root_path
    end
  end

  def edit
    @graph = Graph.find_by(token: params[:token])
    @data = @graph.encrypted ? "#{@graph.data.to_json}" : @graph.data.as_json
  end

  def update
    graph = Graph.find_by(token: params[:token])

    respond_to do |format|
      format.json do
        if graph.update_attributes(data: params[:data], encrypted: params[:encrypted])
          render json: nil, status: 200
        else
          render json: nil, status: 400
        end
      end
    end
  end
end
