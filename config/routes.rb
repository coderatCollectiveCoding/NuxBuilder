Rails.application.routes.draw do
  root    'graphs#new'
  patch   'graphs/:token', to: 'graphs#update'
  get     'graphs/new',    to: 'graphs#new',    as: 'new_graph'
  post    'graphs',        to: 'graphs#create', as: 'create_graph'
  get     'graphs/:token', to: 'graphs#edit',   as: 'edit_graph'
  delete  'graphs/:token', to: 'graphs#destroy'
end
