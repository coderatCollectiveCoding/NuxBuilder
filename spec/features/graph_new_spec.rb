require 'rails_helper'

feature 'Graph new', :js do
  scenario 'creates graph via create button' do
    skip 'link does not react :('
    visit new_graph_path
    find('h3', text: 'Create your own decision tree').trigger('click')
    expect(page).to have_content 'Add choice'
  end
end
