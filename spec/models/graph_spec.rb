require 'rails_helper'

describe Graph do
  context 'ATTRIBUTES' do
    it 'should have unique token' do
      first_graph = FactoryBot.create(:graph)
      second_graph = FactoryBot.build(:graph, token: first_graph.token)

      expect(second_graph).not_to be_valid
    end

    it 'should contain data' do
      graph = build :graph, data: nil

      expect(graph).not_to be_valid
    end
  end
end
