FactoryBot.define do
  factory :graph do
    token { SecureRandom.urlsafe_base64(12) }
    data do
      { data: [
          { id: "1",
            label: "Militant?",
            group: "statement",
            connections: [2, 3]
          },
          { id: "2",
            label: "Ja",
            group: "choice",
            connections: []
          },
          { id: "3",
            label: "Nein",
            group: "choice",
            connections: []
          }
        ]
      }.to_json
    end
  end
end
