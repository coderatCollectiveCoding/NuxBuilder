require 'rails_helper'

RSpec.describe GraphsController, type: :controller do
  context 'POST #create' do
    before do
      @graphs_length = Graph.all.length
      get :create
    end

    it 'Is sucessfull' do
      expect(response.status).to eq(302)
    end

    it 'Can create graph' do
      expect(Graph.all.length).to eq(@graphs_length+1)  
    end

    it 'Should redirect to edit view' do
      expect(response).to redirect_to(edit_graph_path(Graph.last.token))
    end
  end

  context 'GET #new' do
    before do
      get :new
    end

    it 'Is successfull' do
      #todo expect rendered html from new template
      expect(response.status).to eq(200)
    end
  end

  context 'patch #update' do
    before do
      @graph = create :graph
    end

    it 'can update a graph' do
      patch :update, xhr: true, params: { token: @graph.token, data: '42' }
      updated_graph = Graph.find_by(token: @graph.token)

      expect(response.status).to eq(200)
      expect(updated_graph.data).to eq('42')
    end
  end
end
