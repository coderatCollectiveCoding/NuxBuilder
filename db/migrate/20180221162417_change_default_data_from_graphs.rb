class ChangeDefaultDataFromGraphs < ActiveRecord::Migration[5.1]
  def change
    change_column_default :graphs, :data, "{'data':[{'x':-300,'y':-200,'id':'1','connections':[],'label':'My First Statement','group':'statement'},  {'x': -400,'y': -60,'id': '2','connections': ['1'],'label': 'My First Choice','group': 'choice' }]}"
  end
end
