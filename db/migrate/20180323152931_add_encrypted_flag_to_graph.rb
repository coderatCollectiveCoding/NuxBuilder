class AddEncryptedFlagToGraph < ActiveRecord::Migration[5.1]
  def change
    add_column :graphs, :encrypted, :boolean, default: false, nil: false
  end
end
