class AddDefaultDataToGraphs < ActiveRecord::Migration[5.1]
  def change
    change_column_default :graphs, :data, '{[{ "id": "1","text":"My first Statement..","type": "statement","connections": [2, 3]}, { "id": "2","text":"1. Choice","type": "choice","connections": []},{ "id": "3","text":"2. Choice","type": "choice","connections": []}'

  end
end
