class CreateGraphs < ActiveRecord::Migration[5.1]
  def change
    create_table :graphs do |t|
      t.string :token, null: false, unique: true
      t.text :data, null: false

      t.timestamps
    end
  end
end
