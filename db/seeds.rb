Graph.destroy_all

sample_data = {
  data:
  [
    {
      "x": -154,
      "y": 11,
      "id": 1,
      "connections": [
        2
      ],
      "label": "You have a nice walk through the forest...",
      "group": "statement"
    },
    {
      "x": -76,
      "y": -75,
      "id": 2,
      "connections": [
        1,
        3
      ],
      "label": "Left",
      "group": "choice"
    },
    {
      "x": -9,
      "y": -162,
      "id": 3,
      "connections": [
        2,
        4
      ],
      "label": "Where do you want to walk?",
      "group": "statement"
    },
    {
      "x": 125,
      "y": -61,
      "id": 4,
      "connections": [
        3,
        5
      ],
      "label": "Right",
      "group": "choice"
    },
    {
      "x": 154,
      "y": 16,
      "id": 5,
      "connections": [
        4,
        6,
        7
      ],
      "label": "A monster appears. What do you want to do?",
      "group": "statement"
    },
    {
      "x": 202,
      "y": 132,
      "id": 6,
      "connections": [
        8,
        5
      ],
      "label": "Fight!",
      "group": "choice"
    },
    {
      "x": 35,
      "y": 124,
      "id": 7,
      "connections": [
        5,
        9
      ],
      "label": "Run away!",
      "group": "choice"
    },
    {
      "x": 151,
      "y": 260,
      "id": 8,
      "connections": [
        6
      ],
      "label": "You have a good day. The monster is afraid of your fighting skills and runs away...",
      "group": "statement"
    },
    {
      "x": -140,
      "y": 261,
      "id": 9,
      "connections": [
        7
      ],
      "label": "The monster is faster and eats you...",
      "group": "statement"
    }
  ]
}.to_json


3.times do
  Graph.create(
    token: SecureRandom.urlsafe_base64(12),
    data: sample_data,
    encrypted: false
  )
end

# ENCRYPTED TREE, password == 'secret'
encrypted_sample_data = "U2FsdGVkX19l3mjkpz9cGXxG9pVqjqGMnrIwEJc+U8N7p4ysQM/hh3Pgl33rdmOuVktDC2tahtZsxMav3bv1n3VBFcMqsLLCZGruJ0GAryarn2JRPKY0lFdR6DfRklljVMHlTu3PYiTa1IqsFNu+djbRSN/DDf8R+86oaumm9xHJ1M+QFYIE49uAD9WPq59a6iDr8RfKb5gDnFWidTc15knOB55pjJOEjpY75MnIEMVb6mzR0Uz3U4wrU6f5ftT5tQnXz9XfLrtO+RIMjUDMUc0m4IODpsxvFXe3s+qoxn/SCSHfmwfw+TK1i9F/wLhJ1OAt9izJ5flTIs6lzELh75YFhEaqy4ZZig8MYmhZho8uBejnBTf/qGMRJa+MAX07IF0xaObdFvSY7Emc43FRnfNHJfGcIG/PqX1xQmEgsYVM4Ac1N+q7Xk2cZM+IRbDM/KGfF0gESJpQNxlK/575Afm7xkNS+bz5RB4u4qEIzhoR7VG6+cwNx5TgpqqF6hyruAIXMvgMdcqWLCWLLz24nXesn40yU8pheyf3R1FnFFK8dswCGe8/IxQkHRVnSh2+Uz1Qa40KZqcnquJeiH2L76GQKXStNtc+9AfoYfdIx2LzAaTf8qHo38qOxp0elBKbaVP0fGNkuWlvFWnG4TWgCskRHW7S8qLJzt+etpx+e/pMoe+tde3fXOF8FStqWdBJ4j5eIfjEIpws5rQnq14sm5LhY8FhVTPuaL6q2gUDAZ2aSf5Zxyo96dOjeC6+zyaCxbnuFV5Ev936Vdoi6//QkVJ2jIcSd/XClCvYe5bG5YmMaMIvjxYGq0g1qk+YKAD0VHnivi9NLNyFYGZ0dGdmG0xQw7jC2jxWltTO2nseZtjfMUIbN2Xep9os0771VbTE2yt1/U1zzUP9VfGezkG5Yw2g8NpptL6xn8iKUQ2TDUJqOjcykXpM0KjOUJCkyATmFMx6+FJ5yeAQkcAw6LBucv4HaCm/U4neFl22ukkiV45jp+tbsrHyW0jgDVW1l0YnHlyOF+8TiswIV0Bcv/PuEQf1mZokKkVVzvdohuxfvGJueISK0AdhYspECwJ7Y4l7SsIYmF7b18BWh0oKCm0025oRktW4WsIewR01Mbhvna0atqU4V1tZIGvzgUnyrLjL30E3dP0Z7TQcoXHxgaM+xvRaAR5X4Z9f3BeRoEi1KGNgQsV766IqqherMpcJhBL32UtAfM54Y5UY4C+tedhQwaxTEuL8SVNjBKRJmIwv3KRfGhi4yQXMSnxS2HyZnUAG0P37CW6HQGQerg6OdGtJipVsh+ALPE1n5lHNW0ri7MpWMYy6YarND0vCtCxrH8RpEgoERdeXYayCXc+1hnqhIIT+RHPryW+l1zPHn6oyGYOTYW3Gk6GB8ORyMpCONOOhHJTxhoK1ZDdUCp3wnvLCTlPo4+NRx+3ShcqcqlrAlbHYrOwaNOj9wHa6MI7eJxjjQQC7kKMVRXyAuBlWHHq4RKj/FM5t98uqJxriWVwpKa96kgrhyY+RAJVk2ga0WjMg6naXQNP5gBT7IzRdVp4KfddMyURrc66sG9IkmeXsxH65NTE5NSNuZybVULbzePRfKMASi2tQY8GjPN8+ATG3cuGmFOWuy0PxRmkg64fAdTX+3ZHRaU/oc+p+SDONTaDiDQMENHn+0mF7H1CvzjYSGwOOfu8QVF4EFNNNMR/aoexAG984fm1wT9t3N+nUo2Ov0g5N6Enw/MpzVlM0FZwdvchxZlPaOOBKDqczKX9qSQWOORDzS47oVjzoclgvcZbYsgmcZA5l9NuhWmnVY7S0hvmi5BGJZb/TfIrhuqfEiOnOUtCKYwo4qYUp/8qpgvxlaPf7oCvzWXN2LJrWEEbvLWu0vDSO5tSVfZ/iJbU4mfs5hg+TE01OcWYdHdr1dq6OzpCLQDgONVyCh6081nbdswqlEenGsZVclNwvfDHahwd3l2mQwiQGBIxio8mP7wwANFAxhkbOQ/KLqOfBPBpQnA=="

Graph.create(
  token: 'super_encrypted',
  data: encrypted_sample_data,
  encrypted: true
)


