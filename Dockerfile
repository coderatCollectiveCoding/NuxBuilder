FROM ruby:2.4.1-slim

RUN apt-get update -qq && apt-get install -y \
      gnupg2 \
      curl
ADD https://dl.yarnpkg.com/debian/pubkey.gpg /tmp/yarn-pubkey.gpg
RUN apt-key add /tmp/yarn-pubkey.gpg && rm /tmp/yarn-pubkey.gpg
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list
RUN curl -sL https://deb.nodesource.com/setup_8.x |  bash -
RUN apt-get update -qq && apt-get install -y \
    build-essential \
    sqlite3 \
    libsqlite3-dev \
    yarn \
    nodejs \
    git
RUN node -v

RUN mkdir /nuxBuilder
WORKDIR /nuxBuilder
ADD Gemfile /nuxBuilder/Gemfile
ADD Gemfile.lock /nuxBuilder/Gemfile.lock
RUN bundle install
ADD package.json /nuxBuilder/package.json
RUN yarn
